package loghammer

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/rs/xid"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"
	"gitlab.com/buddyspencer/chameleon"
)

type logLevel struct {
	level	 chameleon.Chameleon
	visible  bool
	writable bool
	elastic  bool
	writer   []writer
}

type writer struct {
	logger log.Logger
	color  bool
}

type Elastic struct {
	url 	string
	index 	string
	date	bool
}

var (
	levels = make(map[interface{}]*logLevel)
	Format = "%s[%s] %s"
	ForceColor = true
	DateFormat = "2006-01-02 15:04:05"
	defaultCol = "\x1b[0m"
	file = &os.File{}
	writetolog = false
	err error
	tosyslog = false
	client http.Client
	elastic Elastic
	el_logs []map[string]interface{}
	done = true
	wg sync.WaitGroup
	lock sync.Mutex
	logger []log.Logger
)

func panicf(e string, args ...interface{}) {
	panic(fmt.Sprintf(e, args))
}

func New() {
	logger = append(logger, *log.New(os.Stdout, "", 0))
	levels["info"] = &logLevel{level:chameleon.Lightgreen("info").Bold(), visible:true, writable:true, elastic: true}
	levels["warn"] = &logLevel{level:chameleon.Yellow("warn").Bold(), visible:true, writable:true, elastic: true}
	levels["erro"] = &logLevel{level:chameleon.Lightred("erro").Bold(), visible:true, writable:true, elastic: true}
	levels["pani"] = &logLevel{level:chameleon.Red("pani").Bold(), visible:true, writable:true, elastic: true}
	levels["fata"] = &logLevel{level:chameleon.Red("fata").Bold(), visible:true, writable:true, elastic: true}
	levels["debu"] = &logLevel{level:chameleon.White("debu").Bold(), visible:false, writable:false, elastic: false}
}

func AddLogger(writer io.Writer) {
	logger = append(logger, *log.New(writer, "", 0))
}

func addLevelLogger(level string, w io.Writer, color bool) {
	levels[level].writer = append(levels[level].writer, writer{*log.New(w, "", 0), color})
}

func AddInfoLogger(writer io.Writer, color bool) {
	addLevelLogger("info", writer, color)
}

func AddWarningLogger(writer io.Writer, color bool) {
	addLevelLogger("warn", writer, color)
}

func AddErrorLogger(writer io.Writer, color bool) {
	addLevelLogger("erro", writer, color)
}

func AddFataLogger(writer io.Writer, color bool) {
	addLevelLogger("fata", writer, color)
}

func AddDebugLogger(writer io.Writer, color bool) {
	addLevelLogger("debu", writer, color)
}

func AddLevelLogger(level string, writer io.Writer, color bool) {
	addLevelLogger(level, writer, color)
}

func SetElasticsearchUrl(url, index string, date bool) {
	if !strings.HasSuffix(url, "/") {
		url += "/"
	}
	elastic.url = url
	elastic.index = index
	elastic.date = date
}

func StartElastic() {
	done = false
	wg.Add(1)
	go sendelastic()
}

func sendelastic() {
	for {
		if len(el_logs) < 10 {
			for _, l := range el_logs {
				Send2Elastic(l)
				lock.Lock()
				el_logs = el_logs[1:]
				lock.Unlock()
			}
		} else {
			bulk(el_logs[:10])
			lock.Lock()
			el_logs = el_logs[10:]
			lock.Unlock()
		}
		if len(el_logs) == 0 && done {
			wg.Done()
			return
		}
		time.Sleep(1 * time.Millisecond)
	}
}

func StopElastic() {
	done = true
	wg.Wait()
	return
}

func Send2Elastic(l map[string]interface{}) {
	index := elastic.index
	if elastic.date {
		index += "-" + time.Now().Format("2006-01-02")
	}

	data, err := json.Marshal(l)
	if err != nil {
		elasticError(err)
		return
	}
	url := fmt.Sprintf("%s%s/%s/%s", elastic.url, index, l["type"], l["id"])
	putLog, err := http.NewRequest(http.MethodPut, url, bytes.NewBuffer(data))
	if err != nil {
		elasticError(err)
		return
	}
	putLog.Header.Set("Content-Type", "application/json; charset=utf-8")
	resp, err := client.Do(putLog)
	if err != nil {
		elasticError(err)
		return
	}

	if resp.StatusCode != 201 {
		elasticError("There was a problem. Errorcode from Elasticsearch %d", resp.StatusCode)
		return
	}
}

func elasticError(err interface{}, args ...interface{}) {
	if levels["erro"].elastic {
		levels["erro"].elastic = false
		Errorf(err, args)
		levels["erro"].elastic = true
	} else {
		Errorf(err, args)
	}
}

func bulk(l []map[string]interface{}) {
	data := ""
	for _, log := range l {
		index := elastic.index
		if elastic.date {
			index += "-" + time.Now().Format("2006-01-02")
		}
		d := make(map[string]interface{})
		d["index"] = make(map[string]interface{})
		index_data := make(map[string]interface{})
		index_data["_index"] = index
		index_data["_type"] = log["type"]
		index_data["_id"] = log["id"]
		d["index"] = index_data
		jsonData, err := json.Marshal(d)
		if err != nil {
			Panic(err)
		}
		data += string(jsonData) + "\n"
		jsonData, err = json.Marshal(log)
		if err != nil {
			Panic(err)
		}
		data += string(jsonData) + "\n"
	}

	url := fmt.Sprintf("%s_bulk", elastic.url)
	putLog, err := http.NewRequest(http.MethodPut, url, strings.NewReader(data))
	if err != nil {
		Errorf(err)
		return
	}
	putLog.Header.Set("Content-Type", "application/json; charset=utf-8")
	resp, err := client.Do(putLog)
	if err != nil {
		Errorf(err)
		return
	}

	if resp.StatusCode != 200 {
		Errorf("There was a problem. Errorcode from Elasticsearch %d", resp.StatusCode)
		return
	}
}

func AddLevel(level chameleon.Chameleon) {
	if len(levels) > 0 {
		for l := range levels {
			if l == level.Value() {
				panicf("Level already exists: %s!", level.Value())
			}
		}
		levels[level.Value()] = &logLevel{level:level, visible:true}
	} else {
		panicf("Initialize loghammer first!")
	}
}

func printLevelf(level string, message interface{}, args ...interface{}) {
	if levels[level].visible {
		lvl := level
		if ForceColor {
			lvl = levels[level].level.String()
		}
		for _, l := range logger {
			l.Print(fmt.Sprintf(Format+"\n", lvl, time.Now().Format(DateFormat), fmt.Sprintf(fmt.Sprintf("%v", message), args...)))
		}
		for _, l := range levels[level].writer {
			if !l.color {
				lvl = level
			}
			l.logger.Print(fmt.Sprintf(Format+"\n", lvl, time.Now().Format(DateFormat), fmt.Sprintf(fmt.Sprintf("%v", message), args...)))
		}
	}

	if elastic != (Elastic{}) && levels[level].elastic {
		l := make(map[string]interface{})
		id := xid.New()
		l["level"] = level
		l["message"] = fmt.Sprintf(fmt.Sprintf("%v", message), args...)
		l["timestamp"] = time.Now()
		l["type"] = "logs"
		l["id"] = id.String()
		//Send2Elastic(l)
		lock.Lock()
		el_logs = append(el_logs, l)
		lock.Unlock()
	}
}

func getMessage(message ...interface{}) string {
	msg := ""
	for i, m := range message {
		msg += fmt.Sprint(m)

		if i < len(message) - 1 {
			msg += " "
		}
	}
	return msg
}

func Info(message ...interface{}) {
	printLevelf("info", getMessage(message...))
}

func Infof(message interface{}, args ...interface{}) {
	printLevelf("info", message, args...)
}

func Warn(message ...interface{}) {
	printLevelf("warn", getMessage(message...))
}

func Warnf(message interface{}, args ...interface{}) {
	printLevelf("warn", message, args...)
}

func Error(message ...interface{}) {
	printLevelf("erro", getMessage(message...))
}

func Errorf(message interface{}, args ...interface{}) {
	printLevelf("erro", message, args...)
}

func Fatal(message ...interface{}) {
	printLevelf("fata", getMessage(message...))
	StopElastic()
	os.Exit(1)
}

func Fatalf(message interface{}, args ...interface{}) {
	printLevelf("fata", message, args...)
	StopElastic()
	os.Exit(1)
}

func Panic(message ...interface{}) {
	printLevelf("pani", getMessage(message...))
	StopElastic()
	os.Exit(1)
}

func Panicf(message interface{}, args ...interface{}) {
	printLevelf("pani", message, args...)
	StopElastic()
	os.Exit(1)
}

func Debug(message ...interface{}) {
	printLevelf("debu", getMessage(message...))
}

func Debugf(message interface{}, args ...interface{}) {
	printLevelf("debu", message, args...)
}

func Level(level string, message ...interface{}) {
	printLevelf(level, getMessage(message...))
}

func Levelf(level string, message interface{}, args ...interface{}) {
	printLevelf(level, message, args...)
}

func SetVisibility(level string, visible bool) {
	for l := range levels {
		if l == level {
			levels[level].visible = visible
		}
	}
}

func WriteAllToElastic(write bool) {
	for k := range levels {
		levels[k].elastic = write
	}
}

func WriteLevelToElastic(level string, visible bool) {
	for l := range levels {
		if l == level {
			levels[level].elastic = visible
		}
	}
}

func WriteEverything(write bool) {
	for k := range levels {
		levels[k].writable = write
	}
}

func SetWritable(level string, writeable bool) {
	for l := range levels {
		if l == level {
			levels[level].writable = writeable
		}
	}
}

func SetInfoVisibility(visible bool) {
	SetVisibility("info", visible)
}

func SetWarnVisibility(visible bool) {
	SetVisibility("warn", visible)
}

func SetErrorVisibility(visible bool) {
	SetVisibility("erro", visible)
}

func SetPanicVisibility(visible bool) {
	SetVisibility("pani", visible)
}

func SetFataVisibility(visible bool) {
	SetVisibility("fata", visible)
}

func SetDebugVisibility(visible bool) {
	SetVisibility("debu", visible)
}

func SetInfoWritable(visible bool) {
	SetWritable("info", visible)
}

func SetWarnWritable(visible bool) {
	SetWritable("warn", visible)
}

func SetErrorWritable(visible bool) {
	SetWritable("erro", visible)
}

func SetPanicWritable(visible bool) {
	SetWritable("pani", visible)
}

func SetFataWritable(visible bool) {
	SetWritable("fata", visible)
}

func SetDebugWritable(visible bool) {
	SetWritable("debu", visible)
}

func WriteInfoToElastic(visible bool) {
	WriteLevelToElastic("info", visible)
}

func WriteWarnToElastic(visible bool) {
	WriteLevelToElastic("warn", visible)
}

func WriteErrorToElastic(visible bool) {
	WriteLevelToElastic("erro", visible)
}

func WritePanicToElastic(visible bool) {
	WriteLevelToElastic("pani", visible)
}

func WriteFatalToElastic(visible bool) {
	WriteLevelToElastic("fata", visible)
}

func WriteDebugToElastic(visible bool) {
	WriteLevelToElastic("debu", visible)
}